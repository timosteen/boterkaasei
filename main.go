package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

type Config struct {
	Shift int    `json:"shift"`
	Mode  string `json:"mode"`
}

func main() {
	// Parse command line flags
	configFile := flag.String("config", "", "config.json")
	flag.Parse()

	// Read config file
	configData, err := ioutil.ReadFile(*configFile)
	if err != nil {
		fmt.Println("Error reading config file:", err)
		return
	}

	// Parse config JSON
	var config Config
	err = json.Unmarshal(configData, &config)
	if err != nil {
		fmt.Println("Error parsing config JSON:", err)
		return
	}

	// Validate config
	if config.Shift < 1 || config.Shift > 25 {
		fmt.Println("Invalid shift value in config, must be between 1 and 25")
		return
	}
	if config.Mode != "enc" && config.Mode != "dec" {
		fmt.Println("Invalid mode value in config, must be either 'enc' or 'dec'")
		return
	}

	// Read input text
	fmt.Println("Enter text to (de)crypt:")
	var input string
	fmt.Scanln(&input)
	fmt.Println("Input text:", input)

	// Encrypt or decrypt text based on config
	if config.Mode == "enc" {
		ciphertext := caesarEncrypt(input, config.Shift)
		fmt.Println("Encrypted text:", ciphertext)
	} else {
		plaintext := caesarDecrypt(input, config.Shift)
		fmt.Println("Decrypted text:", plaintext)
	}
}

// Caesar cipher encryption function
func caesarEncrypt(text string, shift int) string {
	var encrypted strings.Builder

	for _, char := range text {
		// If the character is an uppercase letter
		if char >= 'A' && char <= 'Z' {
			encrypted.WriteRune('A' + ((char - 'A' + rune(shift)) % 26))
		}
		// If the character is a lowercase letter
		if char >= 'a' && char <= 'z' {
			encrypted.WriteRune('a' + ((char - 'a' + rune(shift)) % 26))
		} else {
			encrypted.WriteRune(char)
		}
	}

	return encrypted.String()
}

// Caesar cipher decryption function
func caesarDecrypt(text string, shift int) string {
	var decrypted strings.Builder

	for _, char := range text {
		// If the character is an uppercase letter
		if char >= 'A' && char <= 'Z' {
			decrypted.WriteRune('A' + ((char - 'A' - rune(shift) + 26) % 26))
		}
		// If the character is a lowercase letter
		if char >= 'a' && char <= 'z' {
			decrypted.WriteRune('a' + ((char - 'a' - rune(shift) + 26) % 26))
		} else {
			decrypted.WriteRune(char)
		}
	}

	return decrypted.String()
}
